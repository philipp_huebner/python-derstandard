# tests/test_console.py
from collections import namedtuple

import click.testing
import pytest

from derstandard import console


Feed = namedtuple("Feed", ["title"])
FeedData = namedtuple("FeedData", ["feed", "entries"])
FeedEntry = namedtuple("FeedData", ["title", "summary", "published", "link"])


@pytest.fixture()
def runner():
    return click.testing.CliRunner()


@pytest.fixture
def mock_feedparser(mocker):
    mock = mocker.patch("feedparser.parse")
    mock.return_value = FeedData(
        feed=Feed(title="Lorem Ipsum"),
        entries=[
            FeedEntry(
                title="Lorem ipsum",
                summary="Lorem ipsum dolor sit amet",
                published="Thu, 08 Oct 2020 16:48:27 Z",
                link="https://www.derstandard.at/story/2000120605299",
            )
        ],
    )
    return mock


def test_main_succeeds(runner):
    result = runner.invoke(console.main)
    assert result.exit_code == 0


def test_main_succeeds_options(runner):
    result = runner.invoke(console.main, ["--time", "-n", "2"])
    assert result.exit_code == 0


def test_main_fails_on_request_error(runner, mock_feedparser):
    mock_feedparser.side_effect = Exception("Boom")
    result = runner.invoke(console.main)
    assert result.exit_code == 1


def test_main_prints_title(runner, mock_feedparser):
    result = runner.invoke(console.main)
    assert "Lorem Ipsum" in result.output


def test_main_invokes_requests_get(runner, mock_feedparser):
    runner.invoke(console.main)
    assert mock_feedparser.called
