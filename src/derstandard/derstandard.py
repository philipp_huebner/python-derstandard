# src/utils/derstandard.py

from collections import namedtuple
import datetime
import re

import feedparser

RSS_URL = "https://www.derstandard.at/rss"

Feed = namedtuple("Feed", ["title", "len"])
FeedData = namedtuple("FeedData", ["feed", "entries"])
FeedEntry = namedtuple("FeedData", ["title", "summary", "published", "link"])

published_fmt = "%a, %d %b %Y %H:%M:%S Z"


def rssfeed(count=None, time=False):
    def cleanhtml(raw_html):
        # # remove html tags only
        # cleanr = re.compile('<.*?>')
        # remove html tags and html entities like '&nbsp;'
        cleanr = re.compile("<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});")
        cleantext = re.sub(cleanr, "", raw_html)
        return cleantext

    feed = feedparser.parse(RSS_URL)

    if time:  # sort by time
        sorted_entries = sorted(
            feed.entries,
            key=lambda x: datetime.datetime.strptime(x.published, published_fmt),
            reverse=True,
        )
    else:
        sorted_entries = feed.entries

    if count is not None:
        entries = sorted_entries[:count]  # reduce number of entries
    else:
        entries = sorted_entries

    data = FeedData(
        feed=Feed(title=feed.feed.title, len=len(feed.entries)),
        entries=[
            FeedEntry(
                title=e.title,
                summary=cleanhtml(e.summary),
                published=str(datetime.datetime.strptime(e.published, published_fmt)),
                link=e.link.removesuffix("?ref=rss"),
            )
            for e in entries
        ],
    )

    return data
