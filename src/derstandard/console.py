# src/hypermodern_python/console.py

import textwrap

import click

from . import __version__, derstandard


@click.command()
@click.version_option(version=__version__)
@click.option(
    "--count",
    "-n",
    default=None,
    type=int,
    show_default=True,
    help="Number of articles",
)
@click.option("--time", "-t", is_flag=True, help="Sort by published time")
def main(count, time):
    data = derstandard.rssfeed(count=count, time=time)
    col_feedtitle = "\033[0;38;5;198m"
    col_link = "\033[0;2;4;38;5;32m"
    col_dim = "\033[0;2m"
    col_rst = "\033[0m"
    click.echo(col_feedtitle + data.feed.title + col_rst)
    click.echo("\033[0;2mArtikel im RSS feed:" + str(data.feed.len) + col_rst)

    for entry in data.entries:
        click.echo("")
        click.secho(entry.title, fg="white", bold=True)
        click.echo(textwrap.fill(entry.summary))
        click.echo(col_dim + "Link: " + col_link + str(entry.link) + col_rst)
        click.echo(col_dim + "Datum: " + str(entry.published) + col_rst)
