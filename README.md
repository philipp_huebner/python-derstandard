# python-derstandard

A Python command line utility for reading [derStandard.at](https://www.derstandard.at/) RSS feed.


## Installation

```bash
# Install poetry
python3 -m pip install poetry

# Build
poetry build --format=wheel

# Install
python3 -m pip install \
  --no-cache-dir \
  --force-reinstall \
  --only-binary=derstandard \
  dist/${package_name}-0.0.1-py3-none-any.whl

# Run
derstandard --help

```


## Usage

```txt
Usage: derstandard [OPTIONS]

Options:
  --version            Show the version and exit.
  -n, --count INTEGER  Number of articles
  -t, --time           Sort by published time
  --help               Show this message and exit.
```


**Tests:**

```bash
# Run tests
nox -rs tests
```

**Code formatting**

```bash
# Run code formatting
nox -rs black
```

**Linting**

```bash
# Run linting
nox -rs lint
```
